package com.river.classbaithuchanhbai2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerViewJOB;
    ArrayList<JOB> mJOB = new ArrayList<>();
    Button buttonAdd;
    Button buttonChooseTime;
    EditText editTextName;
    EditText editTextSalary;
    RadioGroup radioGroupChooseActive;
    RadioButton radioButtonActive;
    RadioButton radioButtonNotActive;
    JOBAdapter jobAdapter;
    LinearLayout linearLayoutAddNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getView();
        getData();
        setRecyclerView();

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.new_item, null);
                dialogBuilder.setView(dialogView);

                editTextName = dialogView.findViewById(R.id.editTextName);
                editTextSalary = dialogView.findViewById(R.id.editTextSalary);
                radioButtonActive = dialogView.findViewById(R.id.radioButtonActive);
                radioButtonNotActive = dialogView.findViewById(R.id.radioButtonNotActive);
                linearLayoutAddNew = dialogView.findViewById(R.id.linearLayoutAddNew);
                buttonChooseTime = dialogView.findViewById(R.id.buttonTimeActive);
                radioGroupChooseActive = dialogView.findViewById(R.id.radioGroupChooseActive);
                Button buttonSave = dialogView.findViewById(R.id.buttonSave);
                Button buttonCancel = dialogView.findViewById(R.id.buttonCancel);

                AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();

                buttonChooseTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        datePicker();
                    }
                });
                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                buttonSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String status = "Not active";
                        if (radioButtonActive.isSelected()) {
                            status = "Active";
                        }
                        mJOB.add(new JOB(R.drawable.anh1, editTextName.getText().toString(),
                                Double.parseDouble(editTextSalary.getText().toString()), buttonChooseTime.getText().toString(), status));

                        jobAdapter.notifyDataSetChanged();

                        alertDialog.dismiss();
                    }
                });

            }
        });
    }

    String date_time = "";
    int mYear;
    int mMonth;
    int mDay;

    private void datePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        date_time = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        buttonChooseTime.setText(date_time);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void setRecyclerView() {
        jobAdapter = new JOBAdapter(mJOB);
        recyclerViewJOB.setAdapter(jobAdapter);
        recyclerViewJOB.setLayoutManager(new LinearLayoutManager(this));
    }

    private void getData() {
        mJOB.add(new JOB(R.drawable.anh1, "Do Truong Giang", 3000, "1-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh2, "Duong Xuan Son", 1000, "4-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh3, "Tong Thi Dan", 2000, "5-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh4, "Pham Tuan Anh", 3000, "2-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh2, "Nguyen Thanh Sang", 1000, "8-1-2021", "Actived"));

        mJOB.add(new JOB(R.drawable.anh1, "Do Truong Giang", 3000, "1-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh2, "Duong Xuan Son", 1000, "4-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh3, "Tong Thi Dan", 2000, "5-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh4, "Pham Tuan Anh", 3000, "2-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh2, "Nguyen Thanh Sang", 1000, "8-1-2021", "Actived"));

        mJOB.add(new JOB(R.drawable.anh1, "Do Truong Giang", 3000, "1-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh2, "Duong Xuan Son", 1000, "4-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh3, "Tong Thi Dan", 2000, "5-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh4, "Pham Tuan Anh", 3000, "2-1-2021", "Actived"));
        mJOB.add(new JOB(R.drawable.anh2, "Nguyen Thanh Sang", 1000, "8-1-2021", "Actived"));

    }

    private void getView() {
        recyclerViewJOB = findViewById(R.id.recyclerViewJOB);
        buttonAdd = findViewById(R.id.buttonAdd);
    }

}