package com.river.classbaithuchanhbai2;

public class JOB {
    private int avatar;
    private String name;
    private double salary;
    private String date;
    private String active;

    public JOB(int avatar, String name, double salary, String date, String active) {
        this.avatar = avatar;
        this.name = name;
        this.salary = salary;
        this.date = date;
        this.active = active;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
