package com.river.classbaithuchanhbai2;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class JOBViewHolder extends RecyclerView.ViewHolder{
    public ImageView imageViewAvatar;
    public TextView textViewName;
    public TextView textViewSalary;
    public TextView textViewDate;
    public TextView textViewActive;
    public Button buttonXoa;

    public JOBViewHolder(View itemView) {
        super(itemView);

        imageViewAvatar =  itemView.findViewById(R.id.imageAvatar);
        textViewName =  itemView.findViewById(R.id.textViewName);
        textViewSalary =  itemView.findViewById(R.id.textViewSalary);
        textViewDate =  itemView.findViewById(R.id.textViewDate);
        textViewActive =  itemView.findViewById(R.id.textViewActive);
        buttonXoa =  itemView.findViewById(R.id.buttonXoa);

    }
}
