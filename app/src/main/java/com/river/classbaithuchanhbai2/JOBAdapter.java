package com.river.classbaithuchanhbai2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class JOBAdapter extends
    RecyclerView.Adapter<JOBViewHolder> {

    private ArrayList<JOB> mJOB = new ArrayList<>();

    public JOBAdapter(ArrayList<JOB> jobs) {
        mJOB = jobs;
    }

    @NonNull
    @Override
    public JOBViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.item_job, parent, false);
        JOBViewHolder viewHolder = new JOBViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull JOBViewHolder holder, int position) {
        JOB job = mJOB.get(position);
        holder.imageViewAvatar.setImageResource(job.getAvatar());
        holder.textViewName.setText(job.getName());
        holder.textViewSalary.setText(job.getSalary() + "");
        holder.textViewDate.setText(job.getDate());
        holder.textViewActive.setText(job.getActive());
        holder.buttonXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mJOB.remove(position);
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return mJOB.size();
    }

    void submitList(ArrayList<JOB> jobs) {
        mJOB.clear();
        mJOB.addAll(jobs);
        notifyDataSetChanged();
    }
}
